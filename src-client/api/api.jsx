import axios from 'axios';
import { errorToast, infoToast } from '../toast';

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

instance.interceptors.request.use((c) => {
  c.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
  return c;
});

const api = {
  auth: {
    me: () => instance.get('auth/me'),
    login: (data) => instance.post('auth/login', data),
    register: (data) => instance.post('auth/register', data),
  },
  profile: {
    setNotificationSettings: (data) => instance.put('api/users', data),
  },
  notifications: {
    getAll: (data) => instance.get('api/notifications', data),
    updateStatus: () => instance.get('api/notifications/change-status'),
    sendMailNotification: (data) => instance.post('api/users/mail', data),
  },
  files: {
    getAll: () => instance.get('api/files'),
    create: (data) => instance.post('api/files', data),
    destroy: (id) => instance.delete(`api/files/${id}`),
  },
  s3: {
    getPresignedUrl: (data) => instance.post('api/s3', data),
    uploadFile: (file, presignedUrl) => new Promise((resolve, reject) => {
      infoToast('File upload started!');
      const xhr = new XMLHttpRequest();
      xhr.open('PUT', presignedUrl);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.response);
          } else {
            reject(errorToast(`error uploading ${file.type} to S3: ${xhr.status}`));
          }
        }
      };
      xhr.setRequestHeader('Content-Type', file.type);
      xhr.send(file);
    }),
  }
};

export default api;
