import { flow, getEnv } from 'mobx-state-tree';
import { errorToast, infoToast, successToast } from '../../toast';

const FilesStoreActions = (self) => ({
  getAll: flow(function* getAll() {
    const { api } = getEnv(self);
    self.loading = true;
    try {
      const { data } = yield api.files.getAll();
      self.list = data;
    } catch (e) {
      errorToast();
      console.error(e);
    }

    self.loading = false;
  }),
  create: flow(function* create(body) {
    const { api } = getEnv(self);
    self.loading = true;
    try {
      const { data } = yield api.files.create(body);
      self.list = [...self.list, data];
      successToast('File upload completed!');
    } catch (e) {
      errorToast();
      console.error(e);
    }

    self.loading = false;
  }),
  destroy: flow(function* destroy(id) {
    const { api } = getEnv(self);
    self.loading = true;
    try {
      yield api.files.destroy(id);
      self.list = self.list.filter((item) => item._id !== id);
      infoToast('File removed!');
    } catch (e) {
      errorToast();
      console.error(e);
    }

    self.loading = false;
  }),
});

export default FilesStoreActions;
