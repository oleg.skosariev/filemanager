# FileManager


## Configure env

First you need to create a file `.env` following the `.env.example`.

You need to add credentials for the mailer (this is [nodemailer](https://www.npmjs.com/package/nodemailer), the login and password from the gmail and email from which letters will be sent will be enough).

```
NODEMAILER_USER=example@gmail.com
NODEMAILER_PASSWORD=password
NODEMAILER_DEFAULT_SENDER=example@gmail.com
```

You also need to add data from AWS S3: 

```
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
S3_BUCKET=
S3_REGION=
```

## Run application

To run the application, you need to execute the following commands:

```
% docker compose build
% docker compose up
```

## Completed features

* `Sign up` and `Sign in` forms
* a list of notifications with the ability to read them all
* upload, download and remove files
* email notifications
* the ability to configure email notifications (instantly, every 5 minutes, every 10 notifications)
* snapshots and some actions tests
