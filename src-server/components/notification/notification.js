const { model, Schema } = require('mongoose');

const NotificationSchema = new Schema(
  {
    type: {
      required: true,
      type: String,
    },
    isRead: {
      required: false,
      type: Boolean,
      default: false,
    },
    user: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model('Notification', NotificationSchema);
