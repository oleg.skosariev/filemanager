const Notification = require('./notification');

module.exports = () => {
  module.getAll = (id) => Notification.find({ user: id });

  module.changeStatus = async (id) => {
    Notification.updateMany(
      { user: id, isRead: false },
      { $set: { isRead: true } },
      null,
      (error) => {
        if (error) console.log(error);
      }
    );
  };

  return module;
};
