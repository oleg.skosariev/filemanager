const {
  S3Client, PutObjectCommand, GetObjectCommand, DeleteObjectCommand
} = require('@aws-sdk/client-s3');
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner');
const config = require('../../../config/config');

const BUCKET = config.AWS.s3Bucket;
const awsConfig = {
  credentials: {
    secretAccessKey: config.AWS.secretAccessKey,
    accessKeyId: config.AWS.accessKeyId,
  },
  region: config.AWS.s3Region,
  httpOptions: { timeout: 0 },
};

module.exports = () => {
  module.getPresignedUrl = ({ filename, filetype }) => {
    const s3Client = new S3Client(awsConfig);

    const params = {
      Bucket: BUCKET,
      Key: filename,
      ContentType: filetype
    };

    // Create the presigned URL.
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      // Create the command.
      const command = new PutObjectCommand(params);

      // Create the presigned URL.
      const signedUrl = getSignedUrl(s3Client, command, {
        expiresIn: 3600,
      });

      resolve(signedUrl);
    });
  };

  module.putObject = (key, data) => {
    const s3Client = new S3Client(awsConfig);
    const params = {
      Bucket: BUCKET,
      Key: key,
      Body: data,
    };

    const command = new PutObjectCommand(params);
    s3Client.send(command, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`Successfully uploaded data to ${BUCKET}/${key}`);
      }
    });
  };

  module.getObject = (key) => {
    const s3Client = new S3Client(awsConfig);
    const params = {
      Bucket: BUCKET,
      Key: key,
    };

    const streamToString = (stream) => new Promise((resolve, reject) => {
      const chunks = [];
      stream.on('data', (chunk) => chunks.push(chunk));
      stream.on('error', reject);
      stream.on('end', () => resolve(Buffer.concat(chunks)));
    });

    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      const command = new GetObjectCommand(params);
      s3Client.send(command, (err, { Body }) => {
        if (err) {
          console.log(err);
        } else {
          resolve(streamToString(Body));
        }
      });
    });
  };

  module.deleteObject = (key) => {
    const s3Client = new S3Client(awsConfig);
    const params = {
      Bucket: BUCKET,
      Key: key.toString(),
    };

    const command = new DeleteObjectCommand(params);
    s3Client.send(command, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`Successfully deleted from ${BUCKET}/${key}`);
      }
    });
  };

  return module;
};
