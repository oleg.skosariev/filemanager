const { model, Schema } = require('mongoose');

const UserSchema = new Schema(
  {
    email: {
      unique: true,
      required: true,
      type: String,
    },
    password: {
      required: true,
      type: String,
      select: false,
    },
    firstName: {
      required: false,
      type: String,
      select: false,
    },
    lastName: {
      required: false,
      type: String,
      select: false,
    },
    notificationSettings: {
      required: false,
      type: String,
      default: 'instantly',
    },
  },
  {
    timestamps: true,
  }
);

UserSchema.index({ email: 'text' });

module.exports = model('User', UserSchema);
