const User = require('./user');
const { sendMail } = require('../mailer');
const Notification = require('../notification/notification');

module.exports = () => {
  module.setNotificationSettings = (data, user) => User.updateOne({
    _id: user
  }, { notificationSettings: data.settings });

  module.sendMailNotification = async (data, user) => {
    const notification = await Notification.create({
      type: data.template,
      user: user.id
    });
    if (data.sendMail) {
      await sendMail({
        to: user.email, subject: 'Notification', template: data.template, data: {}
      });
    }

    return { notification };
  };

  return module;
};
