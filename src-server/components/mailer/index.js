const ejs = require('ejs');
const path = require('path');

const nodemailer = require('nodemailer');
const config = require('../../../config/config');

const mailer = nodemailer.createTransport({
  host: config.MAIL_SERVICE.host,
  port: config.MAIL_SERVICE.port,
  secure: true,
  auth: {
    user: config.MAIL_SERVICE.username,
    pass: config.MAIL_SERVICE.password,
  },
});

async function sendMail({
  to, subject, template, data
}) {
  const html = await ejs.renderFile(
    path.resolve(__dirname, `../../templates/${template}.ejs`),
    { ...data }
  );

  await mailer.sendMail(
    {
      from: config.MAIL_SERVICE.emailFrom,
      to,
      subject,
      html,
    }
  );
}

module.exports = { sendMail };
