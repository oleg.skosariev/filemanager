const File = require('./file');
const config = require('../../../config/config');
const S3Bucket = require('../s3-bucket');

module.exports = (app) => {
  const s3 = S3Bucket(app);

  module.getAll = (id) => File.find({ user: id });

  module.create = (data, id) => File.create({
    ...data,
    url: `https://${config.AWS.s3Bucket}.s3.${config.AWS.s3Region}.amazonaws.com/${data.name}`,
    user: id
  });

  module.delete = async (id) => {
    const { name } = await File.findById(id);
    File.deleteOne({ _id: id }).then(() => {
      s3.deleteObject(name);
    });
  };

  return module;
};
