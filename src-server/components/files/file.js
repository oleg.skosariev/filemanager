const { model, Schema } = require('mongoose');

const FileSchema = new Schema(
  {
    name: {
      required: true,
      type: String,
    },
    url: {
      required: true,
      type: String,
    },
    size: {
      required: false,
      type: String,
    },
    user: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model('File', FileSchema);
