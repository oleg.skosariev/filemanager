const LocalStrategy = require('passport-local').Strategy;
const _ = require('lodash');
const auth = require('./helpers');
const UserModel = require('../users/user');

module.exports = (app) => new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
async (email, password, cb) => {
  try {
    const user = await UserModel.findOne({ email }).select('+password +lastName +firstName +notificationSettings').exec();

    if (!user || !auth.checkPassword(password, user.password)) {
      cb(null, false, { message: 'Incorrect email or password.' });
      return;
    }
    cb(null, _.omit(user, 'password'), {});
  } catch (err) {
    cb(err);
  }
});
