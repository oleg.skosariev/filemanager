const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt');
const _ = require('lodash');
const config = require('../../../config/constants');
const UserModel = require('../users/user');

module.exports = () => new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwt.secret
},
async (jwtPayload, cb) => {
  try {
    const user = await UserModel.findOne({ email: jwtPayload.email }).select('+firstName +lastName +notificationSettings').exec();

    cb(null, _.omit(user, 'password'));
  } catch (err) {
    cb(err);
  }
});
