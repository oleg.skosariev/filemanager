const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const passport = require('passport');
const cron = require('node-cron');
const apiAuth = require('./routes/auth');
const apiS3 = require('./routes/api/s3-bucket-api');
const apiFiles = require('./routes/api/files-api');
const apiUsers = require('./routes/api/users-api');
const apiNotifications = require('./routes/api/notification-api');
require('./services/db');
const User = require('./components/users/user');
const Notification = require('./components/notification/notification');
const { sendMail } = require('./components/mailer');

module.exports = async () => {
  const app = express();

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'ejs');

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.use(express.static(path.join(__dirname, '../public')));

  // Enable CORS
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

  passport.use(require('./components/auth/local')(app));
  passport.use(require('./components/auth/jwt')(app));

  app.get('/', (req, res) => res.json({ status: 'ok' }));
  app.use('/auth', apiAuth(app));
  app.use('/api', apiS3(app));
  app.use('/api', apiFiles(app));
  app.use('/api', apiUsers(app));
  app.use('/api', apiNotifications(app));

  app.use((req, res, next) => {
    next(createError(404));
  });

  app.use((err, req, res) => {
    res.status(err.status || 500);
    if (err.status === 401) res.send('<!DOCTYPE html><html><head><meta http-equiv="refresh" content="0; url=/auth"></head></html>');
    else res.send(err.message);
  });

  cron.schedule('*/5 * * * *', () => {
    User.find({ notificationType: 'every-5-minutes' }, (err, users) => {
      users.forEach((user) => {
        Notification.find({ user: user._id, isRead: false }, (error, notifications) => {
          if (notifications.length > 0) {
            sendMail({
              to: user.email, subject: 'Notification', template: 'new-notifications', data: {}
            });
          }
        });
      });
    });
  });

  return app;
};
