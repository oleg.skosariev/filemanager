const mongoose = require('mongoose');
const config = require('../../config/config');

mongoose.Promise = Promise;

mongoose.connect(
  config.DB.connectionString,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

module.exports = { db, mongoose };
