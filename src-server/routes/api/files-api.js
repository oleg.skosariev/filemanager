const Router = require('express-promise-router');
const Files = require('../../components/files');
const auth = require('../../components/auth/helpers');

module.exports = (app) => {
  const router = Router();
  const files = Files(app);

  router.get('/', auth.authenticate, async (req, res) => {
    const data = await files.getAll(req.user._id);
    res.json(data);
  });

  router.post('/', auth.authenticate, async (req, res) => {
    const data = await files.create(req.body, req.user._id);
    res.json(data);
  });

  router.delete('/:id', auth.authenticate, async (req, res) => {
    const data = await files.delete(req.params.id);
    res.json(data);
  });

  return Router().use('/files', router);
};
