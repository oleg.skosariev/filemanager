const Router = require('express-promise-router');
const Notifications = require('../../components/notification');
const auth = require('../../components/auth/helpers');

module.exports = () => {
  const router = Router();
  const notifications = Notifications();

  router.get('/', auth.authenticate, async (req, res) => {
    const data = await notifications.getAll(req.user._id);
    res.json(data);
  });

  router.get('/change-status', auth.authenticate, async (req, res) => {
    const data = await notifications.changeStatus(req.user._id);
    res.json(data);
  });

  return Router().use('/notifications', router);
};
