const Router = require('express-promise-router');
const S3Bucket = require('../../components/s3-bucket');
const auth = require('../../components/auth/helpers');

module.exports = (app) => {
  const router = Router();
  const s3 = S3Bucket(app);

  router.post('/', auth.authenticate, async (req, res) => {
    const data = await s3.getPresignedUrl(req.body);
    res.json(data);
  });

  return Router().use('/s3', router);
};
