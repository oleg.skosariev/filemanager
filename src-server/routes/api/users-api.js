const Router = require('express-promise-router');
const Users = require('../../components/users');
const auth = require('../../components/auth/helpers');

module.exports = (app) => {
  const router = Router();
  const users = Users(app);

  router.put('/', auth.authenticate, async (req, res) => {
    const data = await users.setNotificationSettings(req.body, req.user._id);
    res.json(data);
  });

  router.post('/mail', auth.authenticate, async (req, res) => {
    const data = await users.sendMailNotification(req.body, req.user);
    res.json(data);
  });

  return Router().use('/users', router);
};
